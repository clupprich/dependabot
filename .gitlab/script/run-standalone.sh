#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

network="dependabot"
repo_path=$CI_PROJECT_DIR/tmp/repo-contents/testing
app_image="${APP_IMAGE_NAME}/bundler:${CURRENT_TAG}"
gemfile="IyBmcm96ZW5fc3RyaW5nX2xpdGVyYWw6IHRydWUKCnNvdXJjZSAiaHR0cHM6Ly9ydWJ5Z2Vtcy5vcmciCgpnZW0gImZha2VyIiwgIn4+IDEuOSIKZ2VtICJnaXQiLCAifj4gMS44LjAiCmdlbSAicnVib2NvcCIsICJ+PiAxLjE5LjEiCg=="
lockfile="R0VNCiAgcmVtb3RlOiBodHRwczovL3J1YnlnZW1zLm9yZy8KICBzcGVjczoKICAgIGFzdCAoMi40LjIpCiAgICBjb25jdXJyZW50LXJ1YnkgKDEuMS45KQogICAgZmFrZXIgKDEuOS42KQogICAgICBpMThuICg+PSAwLjcpCiAgICBnaXQgKDEuOC4wKQogICAgICByY2hhcmRldCAofj4gMS44KQogICAgaTE4biAoMS44LjExKQogICAgICBjb25jdXJyZW50LXJ1YnkgKH4+IDEuMCkKICAgIHBhcmFsbGVsICgxLjIxLjApCiAgICBwYXJzZXIgKDMuMC4zLjIpCiAgICAgIGFzdCAofj4gMi40LjEpCiAgICByYWluYm93ICgzLjAuMCkKICAgIHJjaGFyZGV0ICgxLjguMCkKICAgIHJlZ2V4cF9wYXJzZXIgKDIuMi4wKQogICAgcmV4bWwgKDMuMi41KQogICAgcnVib2NvcCAoMS4xOS4xKQogICAgICBwYXJhbGxlbCAofj4gMS4xMCkKICAgICAgcGFyc2VyICg+PSAzLjAuMC4wKQogICAgICByYWluYm93ICg+PSAyLjIuMiwgPCA0LjApCiAgICAgIHJlZ2V4cF9wYXJzZXIgKD49IDEuOCwgPCAzLjApCiAgICAgIHJleG1sCiAgICAgIHJ1Ym9jb3AtYXN0ICg+PSAxLjkuMSwgPCAyLjApCiAgICAgIHJ1YnktcHJvZ3Jlc3NiYXIgKH4+IDEuNykKICAgICAgdW5pY29kZS1kaXNwbGF5X3dpZHRoICg+PSAxLjQuMCwgPCAzLjApCiAgICBydWJvY29wLWFzdCAoMS4xNS4wKQogICAgICBwYXJzZXIgKD49IDMuMC4xLjEpCiAgICBydWJ5LXByb2dyZXNzYmFyICgxLjExLjApCiAgICB1bmljb2RlLWRpc3BsYXlfd2lkdGggKDIuMS4wKQoKUExBVEZPUk1TCiAgeDg2XzY0LWRhcndpbi0yMAoKREVQRU5ERU5DSUVTCiAgZmFrZXIgKH4+IDEuOSkKICBnaXQgKH4+IDEuOC4wKQogIHJ1Ym9jb3AgKH4+IDEuMTkuMSkKCkJVTkRMRUQgV0lUSAogICAyLjIuMzEK"

log_with_header "Setup gitlab mock"
log "** Creating '${network}' network **"
docker network create $network

log "** Pulling image '${MOCK_IMAGE}' **"
docker pull --quiet $MOCK_IMAGE

log "** Starting gitlab mock service **"
docker run -d \
  --network $network \
  --name gitlab \
  -p 8080:8080 \
  -p 8081:8081 \
  ${MOCK_IMAGE}

log "** Setting mock expectations **"
set_mock standalone docker

log "** Prepare testing git repo contents **"
setup_git_user
mkdir -p $repo_path
echo $gemfile | base64 -d > $repo_path/Gemfile
echo $lockfile | base64 -d > $repo_path/Gemfile.lock
(cd $repo_path && git init && git add --all && git commit -m "Initial commit")
chown -R 1000:1000 $repo_path

log_with_header "Running standalone gem dependency updates"
log "** Pulling image '${app_image}' **"
docker pull --quiet $app_image

log "** Running rake task 'dependabot:update[testing,bundler,/]' **"
command time -o time.txt -f "%e" docker run --rm -i \
  -e SETTINGS__GITLAB_URL=http://gitlab:8080 \
  -e SETTINGS__GITLAB_ACCESS_TOKEN=e2e-test \
  -e SETTINGS__GITHUB_ACCESS_TOKEN="${GITHUB_ACCESS_TOKEN_TEST:-}" \
  -e SETTINGS__STANDALONE=true \
  -e SETTINGS__LOG_LEVEL=debug \
  -e SETTINGS__LOG_COLOR=true \
  -e SECRET_KEY_BASE=secret \
  -v $repo_path:/home/dependabot/app/tmp/repo-contents/testing \
  --network $network \
  $app_image \
  rake 'dependabot:update[testing,bundler,/]'
