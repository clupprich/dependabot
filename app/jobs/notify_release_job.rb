# frozen_string_literal: true

class NotifyReleaseJob < ApplicationJob
  using Rainbow

  include RunnerHelperConcern

  queue_as :low

  sidekiq_options retry: false

  # Trigger package updates
  #
  # @param [String] name
  # @param [String] package_ecosystem
  # @param [String] project_name
  # @param [Boolean] ignore_rules
  # @return [void]
  def perform(dependency_name, package_ecosystem, project_name, ignore_rules)
    run_within_context({ job: "notify-release", ecosystem: package_ecosystem, dependency: dependency_name }) do
      log(:info, "Triggering updates for #{package_ecosystem.bright}, package #{dependency_name.bright}")
      run_task_in_container(package_ecosystem,
                            "notify_release",
                            [dependency_name, package_ecosystem, project_name, ignore_rules])
    end
  end
end
