# frozen_string_literal: true

module RunnerHelperConcern
  extend ActiveSupport::Concern

  private

  # Run specific task in container
  #
  # @param [String] package_ecosystem
  # @param [String] task_name
  # @param [Array] task_args
  # @return [void]
  def run_task_in_container(package_ecosystem, task_name, task_args)
    container_runner_class.call(
      package_ecosystem: package_ecosystem,
      task_name: task_name,
      task_args: task_args
    )
  end

  # Contaienr runner class
  #
  # @return [Update::Container::Base]
  def container_runner_class
    case UpdaterConfig.deploy_mode
    when UpdaterConfig::K8S_DEPLOYMENT
      Container::Kubernetes::Runner
    when UpdaterConfig::COMPOSE_DEPLOYMENT
      Container::Compose::Runner
    else
      raise("Unsupported deploy mode: #{UpdaterConfig.deploy_mode}")
    end
  end
end
