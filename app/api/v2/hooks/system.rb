# frozen_string_literal: true

module V2
  module Hooks
    class System < API
      helpers do
        include Helpers

        # Check if event is supported
        #
        # @return [Boolean]
        def event_supported?
          %w[project_create project_destroy project_rename project_transfer].include?(params[:event_name])
        end

        # Check if namespace is allowed for project registration
        #
        # @return [Boolean]
        def allowed_namespace?
          return true unless AppConfig.project_registration_allow_pattern

          params[:path_with_namespace].match?(Regexp.new(AppConfig.project_registration_allow_pattern))
        end
      end

      before { authenticate! }

      params do
        requires :event_name, type: String, desc: "Event name"
        optional :path_with_namespace, type: String, desc: "Project full path"
        optional :old_path_with_namespace, type: String, desc: "Project full path before renaming"
      end

      namespace "/hooks/system" do
        desc "Process system hook" do
          detail "Create or update project based on received system webhook event"
          success [
            { model: ::Project::Entity, examples: ::Project::Entity.example_response, message: "Successful response" },
            { code: 204, message: "Skipped, event not supported|Skipped, does not match allowed namespace pattern" }
          ]
        end
        post :project_registration do
          unless event_supported? && allowed_namespace?
            msg = event_supported? ? "does not match allowed namespace pattern" : "event not supported"

            status 202
            return present({ message: "Skipped, #{msg}" })
          end

          present({
            message: Webhooks::SystemHookHandler.call(
              event_name: params[:event_name],
              project_name: params[:path_with_namespace],
              old_project_name: params[:old_path_with_namespace]
            )
          })
        end
      end
    end
  end
end
