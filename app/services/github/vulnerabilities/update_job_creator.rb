# frozen_string_literal: true

module Github
  module Vulnerabilities
    class UpdateJobCreator < ApplicationService
      JOB_NAME = "Vulnerability database update"
      JOB_CLASS = SecurityVulnerabilityUpdateJob

      def call
        if CredentialsConfig.github_access_token
          return if job_synced?

          log(:info, "Creating vulnerability database update job")
          create
        else
          log(:warn, "GitHub access token missing, security vulnerability integration will be disabled")
          destroy # destroy existing jobs in case token is removed from environment
        end
      end

      private

      # Create vulnerability db update jobs for every supported package ecosystem
      # Updates performed every 12 hours
      #
      # @return [void]
      def create
        Sidekiq::Cron::Job.create(job_args)
      end

      # Destroy db update jobs
      #
      # @return [void]
      def destroy
        return unless job

        log(:info, "Removing vulnerability database update job")
        Sidekiq::Cron::Job.destroy(JOB_NAME)
      end

      # Job has up to date arguments
      #
      # @return [Boolean]
      def job_synced?
        job&.to_hash&.values_at(:name, :cron, :klass) == job_args.values_at(:name, :cron, :class) &&
          job&.queue_name_with_prefix == job_args[:queue]
      end

      # All db update jobs created and in sync
      #
      # @return [Boolean]
      def job
        @job ||= Sidekiq::Cron::Job.find(JOB_NAME)
      end

      # Job arguments
      #
      # @return [Hash]
      def job_args
        @job_args ||= {
          name: JOB_NAME,
          cron: "0 1/12 * * *",
          class: JOB_CLASS.name,
          active_job: true,
          queue: JOB_CLASS.queue_name,
          description: "Vulnerability database update"
        }
      end
    end
  end
end
