# frozen_string_literal: true

module Dependabot
  # :reek:TooManyConstants

  # Supported dependabot ecosystems
  #
  class Ecosystem
    BUNDLER = "bundler"
    COMPOSER = "composer"
    GO = "gomod"
    GRADLE = "gradle"
    MAVEN = "maven"
    NPM = "npm"
    PIP = "pip"
    NUGET = "nuget"
    GIT = "gitsubmodule"
    MIX = "mix"
    CARGO = "cargo"
    SWFT = "swift"
    DEVCONTAINERS = "devcontainers"

    PACKAGE_ECOSYSTEM_MAPPING = {
      NPM => "npm_and_yarn",
      GO => "go_modules",
      GIT => "submodules",
      MIX => "hex"
    }.freeze

    PACKAGE_MANAGER_MAPPING = {
      "npm_and_yarn" => NPM,
      "go_modules" => GO,
      "submodules" => GIT,
      "hex" => MIX
    }.freeze
  end
end
