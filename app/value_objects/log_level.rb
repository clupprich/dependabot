# frozen_string_literal: true

# Log level mapper
#
class LogLevel
  class << self
    # Level mapping
    #
    # @return [Hash]
    def levels
      @levels ||= Logger::Severity.constants.to_h { |constant| [constant.to_s, Logger::Severity.const_get(constant)] }
    end

    # Transform log level to number
    #
    # @param [String] level
    # @return [Integer]
    def to_numeric(level)
      levels[level.upcase]
    end

    # Transform log level to string
    #
    # @param [Integer] level
    # @return [String]
    def to_s(level)
      levels.key(level)
    end
  end
end
