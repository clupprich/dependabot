#!/bin/bash

set -e

workspace=$1

source "${workspace}/.gitlab/script/utils.sh"

bash ${workspace}/.gitlab/script/build-core-helpers.sh bundler
echo ""

log_with_header "Update npm packages"
npm install
log_success "success!"
echo ""

log_with_header "Install browsers"
npx playwright install
log_success "success!"
echo ""

log_with_header "Setup database"
bundle exec rake db:create_indexes
log_success "success!"
echo ""

bash ${workspace}/.gitlab/script/add-mock-certs.sh
echo ""

log_success "Setup completed! To start web server and sidekiq process, run 'start_app' command in the terminal."
