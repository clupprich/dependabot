# frozen_string_literal: true

describe Gitlab::Hooks::Remover do
  let(:gitlab) { instance_double(Gitlab::Client, delete_project_hook: nil) }
  let(:project_name) { "project name" }
  let(:webhook_id) { 1 }

  before do
    allow(Gitlab::ClientWithRetry).to receive(:current) { gitlab }
  end

  it "updates existing webhook" do
    described_class.call(project_name, webhook_id: webhook_id)

    expect(gitlab).to have_received(:delete_project_hook).with(project_name, webhook_id)
  end
end
