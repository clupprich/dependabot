# frozen_string_literal: true

describe Github::Vulnerabilities::LocalStore do
  let(:vulnerability) { build(:vulnerability) }

  before do
    allow(Github::Vulnerabilities::Fetcher).to receive(:call).and_return([vulnerability])

    RequestStore.clear!
  end

  context "without github access token" do
    before do
      allow(CredentialsConfig).to receive(:github_access_token).and_return(nil)
    end

    it "skips unsupported package ecosystems" do
      expect(described_class.call("unsupported")).to eq([])
    end

    it "returns empty array without github access token" do
      expect(described_class.call("npm")).to eq([])
    end
  end

  context "with github access token" do
    before do
      allow(CredentialsConfig).to receive(:github_access_token).and_return("token")
    end

    context "with active vulnerabilities" do
      it "fetches vulnerabilities for supported ecosystems" do
        expect(described_class.call("npm")).to eq([vulnerability])
      end
    end

    context "with withdrawn vulnerabilities" do
      before do
        vulnerability.withdrawn_at = Time.current
      end

      it "skips withdrawn vulnerabilities" do
        expect(described_class.call("npm")).to eq([])
      end
    end
  end
end
