# frozen_string_literal: true

describe ProjectsController, :anonymous_access, :integration, type: :request do
  include_context "with api helper"

  let(:project) { create(:project_with_mr) }
  let(:updated_project) { build(:project_with_mr) }

  context "with #index route" do
    it "returns projects page", :aggregate_failures do
      api_get("/projects")

      expect_status(200)
    end
  end

  context "with #update route" do
    before do
      allow(Dependabot::Projects::Creator).to receive(:call).and_return(updated_project)
      allow(Cron::JobSync).to receive(:call)
    end

    it "triggers project sync" do
      put("/projects/#{project.id}")

      expect_status(302)
      expect(Dependabot::Projects::Creator).to have_received(:call).with(
        project.name, access_token: project.gitlab_access_token
      )
      expect(Cron::JobSync).to have_received(:call).with(updated_project)
    end
  end
end
